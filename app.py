

import random, os
from  deck import enum_baralho


class Blackjack():

    i_soma_jogador = 0
    i_soma_mesa = 0

    enum_mao_jogador = {}
    enum_mao_mesa = {}

    b_jogador_obteve_As = False
    b_mesa_obteve_As = False
    vencedor = None
    vez = None
    

    def pedir_carta(self):

        carta = random.choice(list(enum_baralho.items()))
        del enum_baralho[carta[0]]   # remove uma carta do dicionário 

        return carta

    def add_val_carta(self, carta, player):
    
        if player == "jogador":

            if carta[1] == 1:
                self.b_jogador_obteve_As = True  # FLAG, indica que o player recebeu um ás para depois ele decidir o valor se é 1 ou 11

            else:
                self.i_soma_jogador = self.i_soma_jogador + carta[1]

        else:
            if carta[1] == 1:
                self.b_mesa_obteve_As = True

            else:
                self.i_soma_mesa = self.i_soma_mesa + carta[1]

    def finaliza_jogo(self, winner):
        print("\n\n    --$$$$$$$$$$$$  " + winner + " Venceu a rodada  $$$$$$$$$$$\n")

    def jogar(self):
        
        self.vez = "jogador"

        while self.vencedor is None:
            opt = 31416
            val = 0

            mesa_para = False

            while self.vez == "jogador":

                while opt != 1 or opt != 2:
                    opt = int(input("(JOGADOR) escolhe: HIT(1) ou STAND(2): "))

                    if opt == 1 or opt == 2:
                        break
                    
                
                if opt == 1:
                    carta = self.pedir_carta()  
                    print("(JOGADOR) está confiante e puxa outra carta! : " + str(carta))         
                    self.enum_mao_jogador[str(carta[0])] = carta[1]
                    self.add_val_carta(carta, "jogador")
                    

                    if self.b_jogador_obteve_As:
                        val = int(input("(JOGADOR) Ás obtido: 1 ou 11?: "))
                        self.i_soma_jogador = self.i_soma_jogador + val
                        self.b_jogador_obteve_As = False

                    if self.i_soma_jogador == 21:
                        print("(JOGADOR) ...e vence por 21!!! ")
                        self.vencedor = "jogador"
                        self.finaliza_jogo(self.vencedor)
                        self.vez = None

                    if self.i_soma_jogador > 21:
                        print("\n(JOGADOR) ...perde... BUST")
                        self.vencedor = "mesa"
                        self.finaliza_jogo(self.vencedor)
                        self.vez = None


                if opt == 2:
                    self.vez = "mesa"
                    print("(JOGADOR): passa a vez...STAND ")

                    if self.i_soma_jogador > self.i_soma_mesa and mesa_para == True:
                        print("(JOGADOR) vence por " + str(self.i_soma_jogador) + " pontos\nmesa: "+ str(self.i_soma_mesa))
                        self.vencedor = "jogador"
                        self.finaliza_jogo(self.vencedor)
                        self.vez = None

                    elif self.i_soma_jogador < self.i_soma_mesa:
                        self.vencedor = "mesa"
                        self.finaliza_jogo(self.vencedor)
                        self.vez = None

                print("(JOGADOR) Mao atual do Jogador: " + str(self.enum_mao_jogador) + " = " + str(self.i_soma_jogador))

            #### mesa(computador)
            
            while self.vez == "mesa" and mesa_para == False:
                
                # se a mesa fez 17 ou mais pontos, e o jogador tem menos pontos que a mesa... ela para e mantêm as cartas
                if self.i_soma_mesa >= 17 and self.i_soma_jogador < self.i_soma_mesa:
                    self.vez = "jogador"
                    mesa_para = True
                    print("(MESA) para e mantém a mão... ")
                
                # senão a mesa puxa uma carta até ultrapassar 17 e o jogador em pontos
                else:
                    carta = self.pedir_carta()  
                    print("(MESA) está confiante e puxa outra carta! : " + str(carta))         
                    self.enum_mao_mesa[str(carta[0])] = carta[1]
                    self.add_val_carta(carta, "mesa")


                    if self.b_mesa_obteve_As:
                        
                        if 11 + self.i_soma_mesa > 21:
                            self.i_soma_mesa = self.i_soma_mesa + 1
                            print("(MESA) obteve Ás!!, contando como 1, total = "+  str(self.i_soma_mesa))

                        elif 11 + self.i_soma_mesa <= 21:
                            self.i_soma_mesa = self.i_soma_mesa + 11
                            print("(MESA) obteve Ás!!, contando como 1, total = " +  self.i_soma_mesa)

                            self.b_mesa_obteve_As = False
                #

                print("(MESA) Mao atual da mesa: " + str(self.enum_mao_mesa) + " = " + str(self.i_soma_mesa))


                if self.i_soma_mesa == 21:
                    print("(MESA) ...e consegue 21 e vence o jogo!!!!")
                    self.vencedor = "mesa"
                    self.finaliza_jogo(self.vencedor)
                    self.vez = None

                if self.i_soma_mesa > 21:
                    print("(MESA) mas infelizmente passa de 21...")
                    self.vencedor = "jogador"
                    self.finaliza_jogo(self.vencedor)
                    self.vez = None


                #--------------------------



    def comecar_jogo(self):
        
        temp = 0

        print("\n\n\n   - - - - <<<<<<   Blackjack Simulator ©   >>>>>> - - - - -\n")
        

        # entrega das duas cartas
        for i in range(2): 
            
            print("INICIO: vez jogador")
            cartaJogador = self.pedir_carta()  
            print(cartaJogador)         
            self.enum_mao_jogador[str(cartaJogador[0])] = cartaJogador[1]
            self.add_val_carta(cartaJogador, "jogador")


            print("INICIO: vez mesa")
            cartaMesa = self.pedir_carta()
            print(cartaMesa) 
            self.enum_mao_mesa[str(cartaMesa[0])] = cartaMesa[1]
            self.add_val_carta(cartaMesa, "mesa")
        #


        
        # NATURAL / BLACKJACK
        if list(self.enum_mao_jogador.values()) == [1, 10] or list(self.enum_mao_jogador.values()) == [10, 1]:
            self.vencedor = "jogador"
        

        if list(self.enum_mao_mesa.values()) == [1, 10] or list(self.enum_mao_mesa.values()) == [10, 1]:
            self.vencedor = "mesa"
        #

        # checa se houve um NATURAL/BLACKJACK
        if self.vencedor != None:
            self.finaliza_jogo(self.vencedor)

        else:
            # confere a mão inicial dos players para ver se tem Ás
            
            if self.b_jogador_obteve_As == True:
                temp = int(input("Jogador escolhe o valor do Ás: 1 ou 11?: "))

                self.i_soma_jogador = self.i_soma_jogador + temp
                self.b_jogador_obteve_As = False  

            if self.b_mesa_obteve_As == True:

                if 11 + self.i_soma_mesa > 21:
                    self.i_soma_mesa = self.i_soma_mesa + 1
                    print("(MESA) escolhe Ás = 1")
                
                elif 11 + self.i_soma_mesa < 21:
                    self.i_soma_mesa = self.i_soma_mesa + 11
                    print("(MESA) escolhe Ás = 11")
            #


            print("\nmao do jogador: " + str(self.enum_mao_jogador) + " = " + str(self.i_soma_jogador))
            print("mao da mesa: " + str(self.enum_mao_mesa)+ " = " + str(self.i_soma_mesa))

            print("\n")

            # se não houve NATURAL...Comece o jogo.
            if self.vencedor == None:
                self.jogar()
            #


jogo = Blackjack()
jogo.comecar_jogo()

print("jogador: " + str(jogo.i_soma_jogador))
print("mesa: " + str(jogo.i_soma_mesa))
